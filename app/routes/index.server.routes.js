// Add our index routing to our app
module.exports = function(app) {
	var index = require('../controllers/index.server.controller');
	app.get('*', index.render); // catch all sends all requests to index
}