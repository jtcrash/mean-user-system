// Include external modules
var users = require('../../app/controllers/users.server.controller'),
	passport = require('passport');

// Adds all the routes we need for the api/users endpoint (as well as login and logout functionality)
module.exports = function(app) {
	// Login / Logout
	app.get('/api/signout', users.signout);
	app.post('/api/login', users.login);

	// User RESTful interfaces
	app.route('/api/users')
		.post(users.create)
		.get(users.requiresLogin, users.list);

	app.route('/api/users/:userId')
		.get(users.requiresLogin, users.read)
		.put(users.requiresLogin, users.update)
		.delete(users.requiresLogin, users.delete);

	app.param('userId', users.userByID);
};