exports.render = function(req, res) {
	req.session.lastVisit = new Date(); // Timestamp the last session visit

	// Render the page and throw in the logged in user if there is one
	res.render('index', {
		user: JSON.stringify(req.user)
	});
};