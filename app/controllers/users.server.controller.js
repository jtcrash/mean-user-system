// Include external modules
var User = require('mongoose').model('User'),
	passport = require('passport');

// Logs the user in via passport (ultimately creating the session)
// Returns user object
var loginUser = function(req, res, next) {
	passport.authenticate('local', function(err, user, info) {
    	if (err) {
    		return next(err); 
    	}

    	if (user) {
	    	req.logIn(user, function(err) {
	      		if (err) { 
	      			return next(err); 
	      		}

	      		return res.json(user);
	    	});
    	} else {
    		res.json({ error: 1 });
    	}
  	})(req, res, next);
};

// Returns the login status of an user making requests against the API
exports.requiresLogin = function(req, res, next) {
	// Check passports isAuthenticated to see if user has auth'd
	if (!req.isAuthenticated()) {
		return res.status(401).send({
			message: 'You must be logged in to access this area'
		});
	}

	next();
};

// Calls the loginUser method to log the user in via passport
// This method maps directly to the api/login endpoint
exports.login = function(req, res, next) {
  	loginUser(req, res, next);
};

// Uses passport to log the user out (destroying session)
// This method maps directly to the api/logout endpoint
exports.signout = function(req, res) {
	// If we have a user log them out
	if (req.user) {
		req.logout();
		res.status(200).end();
	} else {
		res.status(400).send('No user to log out!');
	}
}

// Using the user model creates a new user in the database
// This method maps directly to the api/users endpoint
exports.create = function(req, res, next) {
	var user = new User(req.body);
	// Check that the access code matches our token stored in config
	// See: /config/env
	if (req.body.accessCode == req.app.get('userCreationToken')) {
		user.save(function(err) {
			if (err) {
				return next(err);
			} else {
				loginUser(req, res, next); // Log the user in if everything is good
			}
		});
	} else {
		res.status(400).send('This access code is invalid or no longer supported');
	}
};

// Renders all users as JSON when requested
// This method maps directly to the api/users endpoint
exports.list = function(req, res, next) {
	User.find({}, function(err, users) {
		if (err) {
			return next(err);
		} else {
			res.json(users);
		}
	});
};

// Returns a single user as JSON when requested
// This method maps directly to the api/users endpoint
exports.read = function(req, res) {
	res.json(req.user);
};

// Grabs the user and registers the user object in the request
exports.userByID = function(req, res, next, id) {
	User.findOne({
		_id: id
	}, function(err, user) {
		if (err) {
			return next(err);
		} else {
			req.user = user;
			next();
		}
	});
};

// Updates a user in the database 
// This method maps directly to the api/users endpoint
exports.update = function(req, res, next) {
	User.findByIdAndUpdate(req.user.id, req.body, function(err, user) {
		if (err) {
			return next(err);
		} else {
			res.json(user);
		}
	});
};

// Removes a user from the database
// this method maps directly to the api/users endpoint
exports.delete = function(req, res, next) {
	req.user.remove(function(err) {
		if (err) {
			return next(err);
		} else {
			res.json(req.user);
		}
	});
};