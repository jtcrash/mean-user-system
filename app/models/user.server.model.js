// Include external modules
var mongoose = require('mongoose'),
	crypto = require('crypto'),
	Schema = mongoose.Schema;

// Define the User object schema 
var UserSchema = new Schema({
	firstName: String,
	lastName: String,
	username: {
		type: String,
		trim: true,
		unique: true,
		required: true
	},
	password: {
		type: String,
		required: true,
		validate: [
			function (password) {
				return password.length >= 6;
			},
			'Password must be 6 or more characters'
		]
	},
	salt: {
		type: String
	},
	provider: {
		type: String,
		required: 'Provider is required'
	},
	providerId: String,
	providerData: {},
	created: {
		type: Date,
		default: Date.now
	}
});

// Before saving we need to salt and hash the password
UserSchema.pre('save', function(next) {
	if (this.password) {
		this.salt = new Buffer(crypto.randomBytes(16).toString('base64'), 'base64');
		this.password = this.hashPassword(this.password);
		next();
	}
});

// Hashes the pasasword for storage
UserSchema.methods.hashPassword = function(password) {
	return crypto.pbkdf2Sync(password, this.salt, 10000, 64).toString('base64');
};

// Checks the requested password against the hash
UserSchema.methods.authenticate = function(password) {
	return this.password === this.hashPassword(password);
};

// Config to allow virtuals and getters
UserSchema.set('toJSON', {
	getters: true,
	virtuals: true 
});

// Creates a computed field in our model (fullName)
UserSchema.virtual('fullName').get(function() {
	return this.firstName + ' ' + this.lastName;
});

mongoose.model('User', UserSchema);