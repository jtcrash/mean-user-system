// RESTful interface for logging a user out via GET
angular.module('users').factory('Logout', ['$resource', function($resource) {
	return $resource('api/signout');
}]);