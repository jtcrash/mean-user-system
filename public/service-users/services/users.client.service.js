// RESTful interface for CRUDing a user
angular.module('users').factory('Users', ['$resource', function($resource) {
	return $resource('api/users/:userId', {
		userId: '@id'
	},{
		update: {
			method: 'PUT'
		}
	});
}]);