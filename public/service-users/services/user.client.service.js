// This module stores the currently logged in users infomration and is responsible for
// determining access throughout the front end application
// TODO: Add function level access
angular.module('users').factory('User', ['$rootScope', '$location', function($rootScope, $location) {
	var currentUser = null;

	return {
		// Checks whether the user has been set
		isLoggedIn: function() {
			return currentUser ? true : false;
		},

		// Returns the user object
		getUser: function() {
			return currentUser;
		},

		// Sets the user object
		setUser: function(user) {
			if (user) {
				currentUser = user;
			}
			$rootScope.$broadcast('userChanged', user);
		}
	};
}]);