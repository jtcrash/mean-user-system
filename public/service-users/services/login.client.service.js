// RESTful interface for logging a user in via POST
angular.module('users').factory('Login', ['$resource', function($resource) {
	return $resource('api/login');
}]);