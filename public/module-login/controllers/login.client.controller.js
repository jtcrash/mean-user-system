// Login Controller
angular.module('login').controller('LoginController', ['$scope', '$location', 'Login', 'User',
	function($scope, $location, Login, User) {
		
		$scope.login = function() {
			$scope.errors = [];

			// Create a new resource to access the login API and POST username and password from the view			
			var user = new Login({
				username: this.username,
				password: this.password
			});

			// Execute the call to login pusing any errors to the error array for display in the view
			user.$save(function(response) {
				if (response.error === 1) {
					$scope.errors.push('Login incorrect, please try again.')
				} else {
					User.setUser(response);
					$location.path('/members');
				}
			}, function(err) {
				console.log(err);
			});
		};

	}
]);