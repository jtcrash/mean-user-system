// Add the login route to our angular app
angular.module('login').config(['$routeProvider', function($routeProvider) {
	
	$routeProvider
		
		.when('/login', {
			templateUrl: 'module-login/views/login.client.view.html'
		})

		.otherwise({
			redirectTo: '/'
		});

}]);