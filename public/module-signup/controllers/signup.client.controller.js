// Signup Cnotroller
angular.module('signup').controller('SignupController', ['$scope', '$location', 'Users', 'User', 
	function($scope, $location, Users, User) {
		
		// Quick method for determining which fields are blank
		// Returns false if all fields are filled in
		var errors = function(fields, scope) {
			var errors = false;
			angular.forEach(fields, function(field) {
				if (!scope[field]) errors = true;
			});

			return errors;
		};

		$scope.signup = function() {
			$scope.errors = [];
			if (!errors(['username', 'firstName', 'lastName', 'accessCode', 'password'], this)) {
				// Check that the password confirmation field matches the password field
				if (this.password == this.passwordConfirm) {
					// Create the resource to hit the API
					var users = new Users({
						username: this.username,
						firstName: this.firstName,
						lastName: this.lastName,
						accessCode: this.accessCode,
						password: this.password,
						provider: 'local'
					});

					// Execute the POST
					users.$save(function(response) {
						// If all goes well set the user here as the user has been logged in server side
						User.setUser(response); 
						$location.path('/members'); // redirect to the members section as we just logged in
					}, function(err) {
						// If we get a 400 (bad request) it is due to the access code not matching
						if (err.status == 400) {
							$scope.errors.push(err.data);
						}
					});
				} else {
					$scope.errors.push('Passwords must match');
				}
			}
		};
	}
]);