// Add the signup routes to the angular route
angular.module('signup').config(['$routeProvider', function($routeProvider) {
	
	$routeProvider
		
		.when('/signup', {
			templateUrl: 'module-signup/views/signup.client.view.html'
		})

		.otherwise({
			redirectTo: '/'
		});

}]);