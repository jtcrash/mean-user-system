var mainApplicationModuleName = 'mean';

// Init the app and inject all needed modules
var mainApplicationModule = angular.module(mainApplicationModuleName, [
	'ngRoute',
	'ngResource',
	'users',
	'login',
	'signup',
	'members',
	'example'
]).controller('MainController', ['$rootScope', '$scope', '$location', '$route', 'User', 'Logout', 
	function($rootScope, $scope, $location, $route, User, Logout) {
		
		// Update the menu if the user changes (logged in or out)
		$scope.$on('userChanged', function(e, user) {
			$scope.user = user;
		});

		// See if we have a user logged in already and commit to the user factory
		if (window.user) {
			User.setUser(window.user);
		}

		// Logout button method for logging the user out (hits the logout service)
		$scope.logout = function() {
			var lo = new Logout;
			lo.$get(function(response) {
				User.setUser(false);
				$location.path('/');
			}, function(err) {
				console.log(err);
			});
		};

		// On route change if we are going to a restricted area check that the user is logged in
		// If we had user roles we would implement that here as well
		$rootScope.$on("$routeChangeStart", function(event, next, current) {
		    if(next.$$route.requireLogin) {
		        if (!User.isLoggedIn()) {
		        	event.preventDefault();
		        	$location.path('/login');
		        }
		    }
		});
		
	}
]);

// Configure router to use hashbang for older browsers
// mainApplicationModule.config(['$locationProvider', function($locationProvider) {
// 	$locationProvider.hashPrefix('!');
// }]);

// Facebook redirect bug (if using oAuth)
// if (window.location.hash === '#_=_') {
// 	window.location.hash = '#!';
// }

// Set the router to HTML5 mode to use history instead of hashes
mainApplicationModule.config(['$locationProvider', function($locationProvider) {
	$locationProvider.html5Mode(true);
}]);

// Bootstrap on ready
angular.element(document).ready(function() {
	angular.bootstrap(document, [mainApplicationModuleName]);
});