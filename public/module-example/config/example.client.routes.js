// Add the example routes to our angular app
angular.module('example').config(['$routeProvider', function($routeProvider) {
	
	$routeProvider
		// We will pull up the example page when users navigate to the root
		.when('/', {
			templateUrl: 'module-example/views/example.client.view.html'
		})

		.otherwise({
			redirectTo: '/'
		});

}]);