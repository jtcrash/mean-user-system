// Add the member route to the angular app (this route requires authentication)
angular.module('members').config(['$routeProvider', function($routeProvider) {
	
	$routeProvider
		
		.when('/members', {
			templateUrl: 'module-members/views/members.client.view.html',
			requireLogin: true
		})

		.otherwise({
			redirectTo: '/'
		});

}]);