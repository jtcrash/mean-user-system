// Members Controller
angular.module('members').controller('MembersController', ['$scope', '$location', 'User', function($scope, $location, User) { 
	$scope.user = User.getUser(); // Send the user information to the view
}]);