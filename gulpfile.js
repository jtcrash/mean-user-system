var gulp = require('gulp');
var buildFolder = 'build/'; // build directory

// Collection of all our scripts
var scripts = [
	'public/lib/less/dist/less.min.js',
	'public/lib/jquery/dist/jquery.min.js',
	'public/lib/bootstrap/dist/js/bootstrap.min.js',
	'public/lib/angular/angular.js',
	'public/lib/angular-route/angular-route.min.js',
	'public/lib/angular-resource/angular-resource.min.js',
	'public/service-*/**/*.js',
	'public/module-*/**/*.js',
	'public/application.js'
];

// All styles that need to be joined and minified (post less compiling paths)
var styles = [
	'public/css/*.css'
];

// Plugins
var less = require('gulp-less');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var mincss = require('gulp-minify-css');
var minhtml = require('gulp-htmlmin');
var copy = require('gulp-copy');
var rename = require('gulp-rename');
var htmlreplace = require('gulp-html-replace');

// Copy over the backend except for the index that we will be modifying
gulp.task('copyBackend', function() {
	gulp.src(['./app/**', '!./app/views/index.ejs', './config/**', 'server.js'])
		//.pipe(ignore('./app/views/index.ejs'))
		.pipe(copy(buildFolder));
});

// Copy over the images directory
gulp.task('copyImgs', function() {
	gulp.src('./public/img/**')
		.pipe(copy(buildFolder));
});

// Copy over the frontend templates
gulp.task('copyTemplates', function() {
	gulp.src('./public/module-*/views/**')
		.pipe(copy(buildFolder));
});

// Compile the less and save it so we can combine with any vendor css
gulp.task('less', function () {
	gulp.src('./public/css/*.less')
		.pipe(less())
		.pipe(gulp.dest('./public/css'));
});

// Minify JS and concat
gulp.task('minjs', function() {
	gulp.src(scripts)
		.pipe(concat('app.js'))
		.pipe(uglify())
		.pipe(gulp.dest(buildFolder + 'public'));
});

// Minify CSS and concat
gulp.task('mincss', function() {
	gulp.src(styles)
		.pipe(concat('styles.css'))
		.pipe(mincss())
		.pipe(gulp.dest(buildFolder + 'public'));
});

// Replace the includes in the index with the new min'd and concat'd files
gulp.task('index', function() {
	gulp.src('app/views/index.ejs')
		.pipe(htmlreplace({
			css: 'styles.css',
			js: 'app.js'
		}))
		.pipe(minhtml({ collapseWhitespace: false }))
		.pipe(gulp.dest(buildFolder + 'app/views/'));
});

// Define our tasks
gulp.task('default', [
	'less', 
	'copyBackend', 
	'copyImgs', 
	'copyTemplates',
	'minjs', 
	'mincss', 
	'index'
]);