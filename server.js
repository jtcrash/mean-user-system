// If the NODE_ENV is not set we default to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

// Pull in our external modules
var mongoose = require('./config/mongoose'), 
	express = require('./config/express'),
	passport = require('./config/passport');

// Init our framework modules
var db = mongoose();
var app = express();
var passport = passport();
var port = 3000;

// If we are on production change the port over to 80
if (process.env.NODE_ENV == 'production') {
	var port = 80;
}

app.listen(port); // start server

// remove the x-powered-by info from the header as it serves no purpose
// besides divuldging more info than needs to be 
app.disable('x-powered-by'); 

module.exports = app;

console.log('Server running on port ' + port);