// Include external modules
var config = require('./config'),
	mongoose = require('mongoose');

// Connect to the database and include our models
module.exports = function() {
	var db = mongoose.connect(config.db);

	// Models
	require('../app/models/user.server.model');
	
	return db;
};