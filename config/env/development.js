// Development environment configs
module.exports = {
	// mongodb://username:password@hotname:port/database
	db: 'mongodb://localhost/mean', // Database connnection string
	sessionSecret: 'Wv2@Ixkwq@5tA3Mb9kj5J*xJ*H28BU7mmx&QA8EQ!6', // Used for encrypting session
	userCreationToken: 's63WGC3!4K6^5qQ6C^d4r2qxsT86qtTN' // Token allowing user to signup (extra auth step)
};