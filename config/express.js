// Include external modules
var config = require('./config'),
	express = require('express'),
	methodOverride = require('method-override'),
	morgan = require('morgan'),
	compress = require('compression'),
	bodyParser = require('body-parser'),
	session = require('express-session'),
	flash = require('connect-flash'),
	passport = require('passport');

module.exports = function() {
	var app = express(); // init express

	// Environment specific setup options
	if (process.env.NODE_ENV === 'development') {
		app.use(morgan('dev'));
	} else if (process.env.NODE_ENV === 'production') {
		app.use(compress());
	}

	// Setup any modules that need to be used
	app.use(bodyParser.urlencoded({
		extended: true
	}));

	app.use(bodyParser.json());
	app.use(methodOverride());
	// app.use(flash());

	// Setup sessions
	app.use(session({
		key: "sessionId",
		saveUninitialized: true,
		resave: true,
		secret: config.sessionSecret
	}));

	// Static user creation token (must be passed to successfully create a user)
	app.set('userCreationToken', config.userCreationToken);

	// Setup for the view/view engine
	app.set('views', './app/views');
	app.set('view engine', 'ejs');

	// Setup passport for user auth
	app.use(passport.initialize());
	app.use(passport.session());

	// Pull in the API specific routing
	require('../app/routes/users.server.routes.js')(app);

	// Setup static file serving
	app.use(express.static('./public'));

	// Lastly include the catch all index routing
	require('../app/routes/index.server.routes.js')(app); 
	
	return app;
};