// Include external modules
var passport = require('passport'),
	mongoose = require('mongoose');

// Configure our passport implementation
module.exports = function() {
	var User = mongoose.model('User');

	// Serialize user for sessions
	passport.serializeUser(function(user, done) {
		done(null, user.id);
	});

	// DeserializeUser when logging out of sessions
	passport.deserializeUser(function(id, done) {
		User.findOne({
			_id: id
		}, '-passport -salt', function(err, user) {
			done(err, user);
		});
	});

	// Strategies for logging in
	require('./strategies/local.js')(); // local strategy
};