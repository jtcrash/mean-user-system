// Loads up whichever env is set via the NODE_ENV variable
// Helps us switch between dev and prod effortlessly
module.exports = require('./env/' + process.env.NODE_ENV + '.js');